// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#pragma once

#include "common.h"
#include "config.h"
#include "random_stream.h"

#if defined(__cplusplus)
extern "C" {
#endif

AUTOJSON typedef struct tag_savedMove {
	float x;
	float y;
	float z;
	float pitch;
	float yaw;
	float roll;
} savedMove;

AUTOJSON typedef struct tag_savedMoves {
	u32 count;
	u32 allocated;
	savedMove *data;
} savedMoves;

typedef enum tag_frameFlag {
	kFrameFlagOutOfData = 0x1,
} frameFlag;

typedef struct tag_vec2d {
	float x;
	float y;
} vec2d;

typedef struct tag_simulationFrame {
	vec2d pos;
	vec2d vel;
	double timestamp;
	u32 id;
	u32 flags;
} simulationFrame;

typedef struct tag_simulationFrames {
	u32 count;
	u32 allocated;
	simulationFrame *data;
} simulationFrames;

typedef struct tag_simulationNode {
	const char *name;
	RandomStream jitterStream;
	RandomStream lossStream;
	simulationFrames frames;
	latencySettings latencySettings;
	nodeSettings nodeSettings;
	u32 color;
	u32 outOfDataColor;
	float targetLatency;

	// for visualization
	u32 lastFrameIndex;

} simulationNode;

typedef struct tag_simulation {
	simulationNode node[kNodeCount];
	simulationConfig config;
	double duration;
	vec2d boundsMin;
	vec2d boundsMax;
	float entityRadius;
	u8 pad[4];
} simulation;

void simulation_init(simulation *sim, simulationConfig *config);
void simulation_generate(simulation *sim);
void simulation_reset(simulation *sim);
void simulation_reset_config(config_t *config);

simulationFrame *simulation_findFrame(simulationFrames *frames, double timestamp, u32 *lastStartIndex);

#if defined(__cplusplus)
}
#endif
