// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#pragma once

void UISimulation_Init();
void UISimulation_Shutdown();
void UISimulation_Update();
