// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#include "app.h"
#include "cmdline.h"
#include "config.h"
#include "fonts.h"
#include "imgui_themes.h"
#include "imgui_utils.h"
#include "message_box.h"
#include "path_utils.h"
#include "process_utils.h"
#include "tasks.h"
#include "theme_config.h"
#include "ui_config.h"
#include "ui_message_box.h"
#include "uuid_config.h"
#include "va.h"
#include "win32_resource.h"
#include "wrap_shellscalingapi.h"

#include "uuid_rfc4122/uuid.h"

#include "bb.h"
#include "bb_array.h"
#include "bb_wrap_stdio.h"

BB_WARNING_PUSH(4820)
#include <shlobj_core.h>
BB_WARNING_POP

#include "ui_simulation.h"

globals_t globals;
bool g_shuttingDown;
bool g_failedDiscoveryStartup;

extern "C" {
void get_appdata_folder(char *buffer, size_t bufferSize)
{
	size_t len;
	char appData[_MAX_PATH] = "C:";
	SHGetFolderPathA(NULL, CSIDL_LOCAL_APPDATA, NULL, 0, appData);
	len = strlen(appData);
	bb_strncpy(buffer, appData, bufferSize);
	bb_strncpy(buffer + len, "\\nettest", bufferSize - len);
	path_mkdir(buffer);
}
}

static bool s_showDemo;
static char s_imguiPath[1024];
static bool app_get_imgui_path(char *buffer, size_t bufferSize)
{
	get_appdata_folder(buffer, bufferSize);
	size_t dirLen = strlen(buffer);

	if(bb_snprintf(buffer + dirLen, bufferSize - dirLen, "/imgui.ini") < 0) {
		buffer[bufferSize - 1] = '\0';
	}

	return true;
}

HRESULT SetProcessDpiAwarenessShim(_In_ PROCESS_DPI_AWARENESS value)
{
	HMODULE hModule = GetModuleHandleA("shcore.dll");
	if(hModule) {
		typedef HRESULT(WINAPI * Proc)(_In_ PROCESS_DPI_AWARENESS value);
		Proc proc = (Proc)(void *)(GetProcAddress(hModule, "SetProcessDpiAwareness"));
		if(proc) {
			return proc(value);
		}
	}
	return STG_E_UNIMPLEMENTEDFUNCTION;
}

static bool App_CreateWindow(void)
{
	extern LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	if(g_config.dpiAware) {
		SetProcessDpiAwarenessShim(PROCESS_PER_MONITOR_DPI_AWARE);
	}

	const char *classname = "Nettest";
	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_MAINICON)), LoadCursor(NULL, IDC_ARROW), NULL, NULL, classname, NULL };
	globals.wc = wc;
	RegisterClassEx(&globals.wc);

	WINDOWPLACEMENT wp = g_config.wp;
	const char *title = classname;
	globals.hwnd = CreateWindow(classname, title, WS_OVERLAPPEDWINDOW, 100, 100, 1280, 800, NULL, NULL, globals.wc.hInstance, NULL);
	if(wp.rcNormalPosition.right > wp.rcNormalPosition.left) {
		if(wp.showCmd == SW_SHOWMINIMIZED) {
			wp.showCmd = SW_SHOWNORMAL;
		}
		SetWindowPlacement(globals.hwnd, &wp);
	}
	return globals.hwnd != 0;
}

bool App_Init(const char *cmdline)
{
	cmdline_init_composite(cmdline);
	app_get_imgui_path(s_imguiPath, sizeof(s_imguiPath));
	ImGuiIO &io = ImGui::GetIO();
	io.IniFilename = s_imguiPath;

	const char *applicationName = u8"Nettest";
	BB_INIT(applicationName);
	BB_THREAD_SET_NAME("main");
	BB_LOG("Startup", "%s starting...\n", applicationName);

	bbthread_set_name("main");
	process_init();
	tasks_startup();
	uuid_init(&uuid_read_state, &uuid_write_state);
	config_read(&g_config);
	Style_Init();
	UISimulation_Init();
	return App_CreateWindow();
}

void App_Shutdown()
{
	UISimulation_Shutdown();
	Style_ResetConfig();
	mb_shutdown();
	tasks_shutdown();
	UIConfig_Reset();
	config_write(&g_config);
	config_reset(&g_config);
	uuid_shutdown();
	BB_SHUTDOWN();

	if(globals.hwnd) {
		DestroyWindow(globals.hwnd);
	}
	if(globals.wc.hInstance) {
		UnregisterClass(globals.wc.lpszClassName, globals.wc.hInstance);
	}
	cmdline_shutdown();
}

int g_appRequestRenderCount;
extern "C" void App_RequestRender(void)
{
	g_appRequestRenderCount = 3;
}
extern "C" bool App_GetAndClearRequestRender(void)
{
	bool ret = g_appRequestRenderCount > 0;
	g_appRequestRenderCount = BB_MAX(0, g_appRequestRenderCount - 1);
	return ret;
}

void App_Update()
{
	BB_TICK();
	tasks_tick();
	if(ImGui::BeginMainMenuBar()) {
		if(ImGui::BeginMenu("File")) {
			if(ImGui::MenuItem("Exit")) {
				g_shuttingDown = true;
			}
			ImGui::EndMenu();
		}
		if(!UIConfig_IsOpen()) {
			if(ImGui::BeginMenu("Edit")) {
				if(ImGui::MenuItem("Preferences")) {
					BB_LOG("UI::Menu::Config", "UIConfig_Open");
					UIConfig_Open(&g_config);
				}
				ImGui::EndMenu();
			}
		}
		if(ImGui::BeginMenu("Debug")) {
			if(ImGui::MenuItem("DEBUG Reload style colors")) {
				Style_Apply();
			}
			if(ImGui::BeginMenu("DEBUG Scale")) {
				void QueueUpdateDpiDependentResources();
				if(ImGui::MenuItem("1")) {
					g_config.dpiScale = 1.0f;
					QueueUpdateDpiDependentResources();
				}
				if(ImGui::MenuItem("1.25")) {
					g_config.dpiScale = 1.25f;
					QueueUpdateDpiDependentResources();
				}
				if(ImGui::MenuItem("1.5")) {
					g_config.dpiScale = 1.5f;
					QueueUpdateDpiDependentResources();
				}
				if(ImGui::MenuItem("1.75")) {
					g_config.dpiScale = 1.75f;
					QueueUpdateDpiDependentResources();
				}
				if(ImGui::MenuItem("2")) {
					g_config.dpiScale = 2.0f;
					QueueUpdateDpiDependentResources();
				}
				ImGui::EndMenu();
			}
			Fonts_Menu();
			ImGui::EndMenu();
		}
		if(ImGui::BeginMenu("Help")) {
			if(ImGui::MenuItem("Demo")) {
				BB_LOG("UI::Menu::Demo", "s_showDemo -> %d", !s_showDemo);
				s_showDemo = !s_showDemo;
			}
			ImGui::EndMenu();
		}

		ImGui::EndMainMenuBar();
	}

	UISimulation_Update();

	if(s_showDemo) {
		ImGui::ShowTestWindow();
	}
	UIConfig_Update(&g_config);
	UIMessageBox_Update();
}

bool App_IsShuttingDown()
{
	return g_shuttingDown;
}
