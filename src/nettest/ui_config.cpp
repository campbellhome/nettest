// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#include "ui_config.h"
#include "app.h"
#include "fonts.h"
#include "imgui_themes.h"
#include "imgui_utils.h"

#include "bb_array.h"
#include "bb_string.h"

using namespace ImGui;
void QueueUpdateDpiDependentResources();

static config_t s_preferencesConfig;
bool s_preferencesValid;
bool s_preferencesOpen;
bool s_preferencesNeedFocus;

void UIConfig_Open(config_t *config)
{
	if(s_preferencesValid) {
		config_reset(&s_preferencesConfig);
		s_preferencesValid = false;
	}

	s_preferencesConfig = config_clone(config);
	s_preferencesValid = true;
	s_preferencesOpen = true;
	s_preferencesNeedFocus = true;
}

void UIConfig_Reset()
{
	if(s_preferencesValid) {
		config_reset(&s_preferencesConfig);
		s_preferencesValid = false;
		s_preferencesOpen = false;
	}
}

bool UIConfig_IsOpen()
{
	return s_preferencesOpen;
}

static const char *s_colorschemes[] = {
	"ImGui Dark",
	"Light",
	"Classic",
	"Visual Studio Dark",
	"Windows",
};

void UIConfig_ApplyColorscheme(config_t *config)
{
	if(!config) {
		config = &g_config;
	}
	const char *colorscheme = sb_get(&config->colorscheme);
	Style_Reset(colorscheme);
}

void UIConfig_Update(config_t *config)
{
	if(!s_preferencesOpen)
		return;

	float startY = ImGui::GetFrameHeight();
	ImGuiIO &io = ImGui::GetIO();
	SetNextWindowSize(ImVec2(io.DisplaySize.x, io.DisplaySize.y - startY), ImGuiSetCond_Always);
	SetNextWindowPos(ImVec2(0, startY), ImGuiSetCond_Always);

	if(Begin("Preferences", &s_preferencesOpen, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoTitleBar)) {
		if(ImGui::CollapsingHeader("Interface", ImGuiTreeNodeFlags_DefaultOpen)) {
			InputFloat("Double-click seconds", &s_preferencesConfig.doubleClickSeconds);

			int colorschemeIndex = -1;
			for(int i = 0; i < BB_ARRAYSIZE(s_colorschemes); ++i) {
				if(!strcmp(sb_get(&s_preferencesConfig.colorscheme), s_colorschemes[i])) {
					colorschemeIndex = i;
					break;
				}
			}
			if(ImGui::Combo("Colorscheme", &colorschemeIndex, s_colorschemes, BB_ARRAYSIZE(s_colorschemes))) {
				if(colorschemeIndex >= 0 && colorschemeIndex < BB_ARRAYSIZE(s_colorschemes)) {
					sb_reset(&s_preferencesConfig.colorscheme);
					sb_append(&s_preferencesConfig.colorscheme, s_colorschemes[colorschemeIndex]);
					UIConfig_ApplyColorscheme(&s_preferencesConfig);
				}
			}
			ImGui::Checkbox("DPI Aware", &s_preferencesConfig.dpiAware);
			if(ImGui::IsItemHovered()) {
				ImGui::SetTooltip("Requires restart.  Default font is not recommended if DPI Aware.");
			}
		}
		ImFont *uiFont = ImGui::GetFont();
		ImVec2 fontSizeDim = uiFont->CalcTextSizeA(uiFont->FontSize, FLT_MAX, 0.0f, "100 _+__-_ size");
		if(ImGui::CollapsingHeader("Font", ImGuiTreeNodeFlags_DefaultOpen)) {
			BeginGroup();
			PushID("UIFont");
			Checkbox("Custom UI Font", &s_preferencesConfig.uiFontConfig.enabled);
			if(s_preferencesConfig.uiFontConfig.enabled) {
				int val = (int)s_preferencesConfig.uiFontConfig.size;
				PushItemWidth(fontSizeDim.x);
				InputInt("size", &val, 1, 10);
				PopItemWidth();
				val = BB_CLAMP(val, 1, 1024);
				s_preferencesConfig.uiFontConfig.size = (u32)val;
				Text("Path:");
				SameLine();
				PushItemWidth(300.0f * g_config.dpiScale);
				ImGuiInputTextFlags flags = ImGuiInputTextFlags_AutoSelectAll | ImGuiInputTextFlags_EnterReturnsTrue;
				InputText("##path", &s_preferencesConfig.uiFontConfig.path, kBBSize_MaxPath, flags);
				Fonts_CacheGlyphs(sb_get(&s_preferencesConfig.uiFontConfig.path));
				if(IsItemActive() && App_HasFocus()) {
					App_RequestRender();
				}
				PopItemWidth();
			}
			PopID();
			EndGroup();
		}
		Separator();
		if(Button("Ok")) {
			WINDOWPLACEMENT wp = config->wp;
			config_t tmp = *config;
			*config = s_preferencesConfig;
			config->wp = wp;
			s_preferencesConfig = tmp;
			s_preferencesOpen = false;
			GetIO().MouseDoubleClickTime = config->doubleClickSeconds;
			QueueUpdateDpiDependentResources();
		}
		SameLine();
		if(Button("Cancel")) {
			s_preferencesOpen = false;
		}
	}
	End();
	if(!s_preferencesOpen && s_preferencesValid) {
		config_reset(&s_preferencesConfig);
		s_preferencesValid = false;
	}
}
