// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#include "bb.h"
#include "sb.h"

// define the Windows structs for preproc
#if 0
AUTOJSON typedef struct tagPOINT {
	LONG x;
	LONG y;
} POINT;

AUTOJSON typedef struct tagRECT {
	LONG left;
	LONG top;
	LONG right;
	LONG bottom;
} RECT;

AUTOJSON typedef struct tagWINDOWPLACEMENT {
	UINT length;
	UINT flags;
	UINT showCmd;
	POINT ptMinPosition;
	POINT ptMaxPosition;
	RECT rcNormalPosition;
} WINDOWPLACEMENT;
#endif

AUTOJSON typedef struct fontConfig_s {
	b32 enabled;
	u32 size;
	sb_t path;
} fontConfig_t;

typedef enum tag_nodeType {
	kNodeAutonomous,
	kNodeAuthority,
	kNodeSimulated,
	kNodeLerp,
	kNodeExpLerp,
	kNodeLerp2,
	kNodeCount
} nodeType;

AUTOJSON typedef struct tag_latencySettings {
	double latencyMsec;
	double jitterMsec;
	double loss01;
	b32 outOfOrder;
	u8 pad[4];
} latencySettings;

AUTOJSON typedef struct tag_nodeSettings {
	double sendIntervalMillis;
	float tuning;
	float maxExtrapolationMillis;
	float extrapolationLerpStrength;
	b32 visible;
} nodeSettings;

AUTOJSON typedef struct tag_simulationConfig {
	latencySettings latency[2];
	nodeSettings node[kNodeCount];
	double duration;
	double speed;
	b32 savedPath;
	b32 showVelocity;
	b32 showFacing;
	b32 showCapsule;
	b32 showExtrapolation;
	b32 useExtrapolation;
} simulationConfig;

AUTOJSON typedef struct config_s {
	simulationConfig simulation;
	fontConfig_t uiFontConfig;
	sb_t colorscheme;
	WINDOWPLACEMENT wp;
	u32 version;
	b32 dpiAware;
	float doubleClickSeconds;
	float dpiScale;
	u8 pad[4];
} config_t;

enum { kConfigVersion = 4 };

extern config_t g_config;

b32 config_read(config_t *config);
b32 config_write(config_t *config);
config_t config_clone(config_t *config);
void config_reset(config_t *config);
void config_free(config_t *config);
void config_getwindowplacement(HWND hwnd);

#if defined(__cplusplus)
}
#endif
