// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#include "ui_simulation.h"
#include "app.h"
#include "imgui_utils.h"
#include "simulation.h"
#include "time_utils.h"

static simulation s_sim;

struct simulationPlayback {
	double rate = 1.0;
	double timestamp;
};
static simulationPlayback s_playback;

static void UISimulation_InitColors(simulation *sim)
{
	sim->node[kNodeAutonomous].color = IM_COL32(51, 255, 240, 255);
	sim->node[kNodeAuthority].color = IM_COL32(0, 128, 128, 255);
	sim->node[kNodeSimulated].color = IM_COL32(128, 128, 0, 255);
	sim->node[kNodeLerp].color = IM_COL32(128, 0, 128, 255);
	sim->node[kNodeExpLerp].color = IM_COL32(128, 0, 0, 255);
	sim->node[kNodeLerp2].color = IM_COL32(128, 128, 128, 255);
	sim->node[kNodeLerp2].outOfDataColor = IM_COL32(255, 255, 0, 255);
}

void UISimulation_Init()
{
	simulation *sim = &s_sim;

	simulation_init(sim, &g_config.simulation);
	simulation_generate(sim);
	UISimulation_InitColors(sim);
}

void UISimulation_Shutdown()
{
	g_config.simulation = s_sim.config;
	for(int i = 0; i < 2; ++i) {
		g_config.simulation.latency[i] = s_sim.node[i].latencySettings;
	}
	for(int i = 0; i < kNodeCount; ++i) {
		g_config.simulation.node[i] = s_sim.node[i].nodeSettings;
	}
	simulation_reset(&s_sim);
}

ImVec2 operator+(const ImVec2 &a, const ImVec2 &b)
{
	ImVec2 result(a.x + b.x, a.y + b.y);
	return result;
}

ImVec2 operator-(const ImVec2 &a, const ImVec2 &b)
{
	ImVec2 result(a.x - b.x, a.y - b.y);
	return result;
}

ImVec2 operator*(const ImVec2 &a, float b)
{
	ImVec2 result(a.x * b, a.y * b);
	return result;
}

ImVec2 operator/(const ImVec2 &a, float b)
{
	ImVec2 result(a.x / b, a.y / b);
	return result;
}

static void UISimulation_DrawSimulationFrame(ImVec2 canvasMin, double timestamp, bool showX, bool showY, simulationNode *node, float simScale, ImVec2 simMin)
{
	simulationFrames *frames = &node->frames;
	u32 color = node->color;
	u32 *lastIndex = &node->lastFrameIndex;

	ImDrawList *drawlist = ImGui::GetWindowDrawList();
	simulationFrame *frame = simulation_findFrame(frames, timestamp, lastIndex);
	if(frame) {
		if((frame->flags & kFrameFlagOutOfData) != 0 && g_config.simulation.showExtrapolation) {
			color = node->outOfDataColor;
		}
		ImVec2 pos(canvasMin);
		if(showX) {
			pos.x += (frame->pos.x - simMin.x) * simScale;
		}
		if(showY) {
			pos.y += (frame->pos.y - simMin.y) * simScale;
		}
		drawlist->AddCircleFilled(pos, s_sim.entityRadius * simScale, color, (int)(s_sim.entityRadius));
		if(g_config.simulation.showVelocity && (frame->vel.x != 0.0f || frame->vel.y != 0.0f)) {
			ImVec2 end(pos);
			end.x += frame->vel.x * simScale;
			end.y += frame->vel.y * simScale;
			drawlist->AddLine(pos, end, color);
		}
	}
}

static void UISimulation_Draw(ImVec2 canvasStart, ImVec2 canvasEnd, float simScale, bool showX, bool showY)
{
	ImDrawList *drawlist = ImGui::GetWindowDrawList();
	drawlist->AddRectFilled(canvasStart, canvasEnd, 0xff181818);

	ImVec2 avail = canvasEnd - canvasStart;
	ImVec2 simSize = { s_sim.boundsMax.x - s_sim.boundsMin.x, s_sim.boundsMax.y - s_sim.boundsMin.y };
	ImVec2 simMin = { s_sim.boundsMin.x, s_sim.boundsMin.y };
	ImVec2 simUsed = simSize * simScale;
	ImVec2 offset = { (avail.x - simUsed.x) * 0.5f, (avail.y - simUsed.y) * 0.5f };
	if(!showX) {
		offset.x = avail.x * 0.5f;
	} else if(!showY) {
		offset.y = avail.y * 0.5f;
	}

	for(u32 i = 0; i < kNodeCount; ++i) {
		if(s_sim.node[i].nodeSettings.visible) {
			UISimulation_DrawSimulationFrame(canvasStart + offset, s_playback.timestamp, showX, showY, s_sim.node + i, simScale, simMin);
		}
	}
}

bool DoubleVal(double *val, double minVal, double maxVal, const char *name)
{
	bool changed = false;
	ImGui::PushID(name);
	ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth() * 0.25f);
	if(ImGui::InputDouble("##Input", val, 0.0, 0.0, "%.6f", ImGuiInputTextFlags_EnterReturnsTrue)) {
		changed = true;
	}
	ImGui::SameLine();
	if(ImGui::SliderScalar("##Slider", ImGuiDataType_Double, val, &minVal, &maxVal)) {
		changed = true;
	}
	ImGui::PopItemWidth();
	ImGui::SameLine();
	ImGui::Text("%s", name);
	ImGui::PopID();
	return changed;
};

bool FloatVal(float *val, float minVal, float maxVal, const char *name)
{
	bool changed = false;
	ImGui::PushID(name);
	ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth() * 0.25f);
	if(ImGui::InputFloat("##Input", val, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue)) {
		changed = true;
	}
	ImGui::SameLine();
	if(ImGui::SliderScalar("##Slider", ImGuiDataType_Float, val, &minVal, &maxVal)) {
		changed = true;
	}
	ImGui::PopItemWidth();
	ImGui::SameLine();
	ImGui::Text("%s", name);
	ImGui::PopID();
	return changed;
};

void UISimulation_Update()
{
	if(ImGui::Begin("Simulation", (bool *)nullptr, 0)) {
		double rate = s_playback.rate;
		if(ImGui::InputDouble("Rate", &rate, 0.0, 0.0, "%.6f", ImGuiInputTextFlags_EnterReturnsTrue)) {
			s_playback.rate = rate;
		}

		if(s_playback.rate > 0.0) {
			s_playback.timestamp += Time_GetDTDouble() * s_playback.rate * 1000.0;
			while(s_playback.timestamp > s_sim.duration) {
				s_playback.timestamp -= s_sim.duration;
			}
			App_RequestRender();
		}

		float timestamp = (float)s_playback.timestamp;
		if(ImGui::SliderFloat("Position", &timestamp, 0.0f, (float)s_sim.duration)) {
			s_playback.timestamp = timestamp;
		}

		if(ImGui::Checkbox("Show velocity", &g_config.simulation.showVelocity)) {
			s_sim.config.showVelocity = g_config.simulation.showVelocity;
		}

		ImGui::SameLine();
		if(ImGui::Checkbox("Show extrapolation", &g_config.simulation.showExtrapolation)) {
			s_sim.config.showExtrapolation = g_config.simulation.showExtrapolation;
		}

		for(u32 i = 0; i < kNodeCount; ++i) {
			simulationNode *node = s_sim.node + i;
			if(ImGui::Checkbox(node->name, &g_config.simulation.node[i].visible)) {
				node->nodeSettings.visible = g_config.simulation.node[i].visible;
			}
			if(((i + 1) % 3) != 0 && i + 1 < kNodeCount) {
				ImGui::SameLine();
			}
		}

		ImVec2 windowPos = ImGui::GetWindowPos();
		ImVec2 avail = ImGui::GetContentRegionAvail();
		ImVec2 region = ImGui::GetWindowContentRegionMax();

		ImVec2 base(windowPos + region - avail);
		ImVec2 canvasSize(avail - ImVec2(20, 20));

		ImVec2 simSize = { s_sim.boundsMax.x - s_sim.boundsMin.x, s_sim.boundsMax.y - s_sim.boundsMin.y };
		ImVec2 simScale = { canvasSize.x / simSize.x, canvasSize.y / simSize.y };
		float finalSimScale = BB_MIN(simScale.x, simScale.y);

		UISimulation_Draw(base, base + canvasSize, finalSimScale, true, true);

		ImVec2 vertStart(base + ImVec2(canvasSize.x + 2.0f, 0.0f));
		ImVec2 vertEnd(base + canvasSize + ImVec2(20, 0));
		UISimulation_Draw(vertStart, vertEnd, finalSimScale, false, true);

		ImVec2 horizStart(base + ImVec2(0.0f, canvasSize.y + 2.0f));
		ImVec2 horizEnd(base + canvasSize + ImVec2(0, 20));
		UISimulation_Draw(horizStart, horizEnd, finalSimScale, true, false);
	}
	ImGui::End();

	bool settingsChanged = false;

	if(ImGui::Begin("Latency Settings", (bool *)nullptr, 0)) {
		auto NodeSettings = [](simulationNode *node, simulationConfig *config, u32 nodeIndex) {
			double sendInterval[] = { 1.0, 100.0 };
			double latency[] = { 1.0, 1000.0 };
			double jitter[] = { 0.0, 100.0 };
			double loss[] = { 0.0, 1.0 };
			ImGui::PushID(node->name);
			ImGui::TextColored(ImColor(node->color), "%s", node->name);
			bool anyChanged = false;
			anyChanged = DoubleVal(&config->node[nodeIndex].sendIntervalMillis, 1.0, 100.0, "Send Interval (ms)") || anyChanged;
			anyChanged = DoubleVal(&config->latency[nodeIndex].latencyMsec, 1.0, 1000.0, "Latency (ms)") || anyChanged;
			anyChanged = DoubleVal(&config->latency[nodeIndex].jitterMsec, 0.0, 100.0, "Jitter (ms)") || anyChanged;
			anyChanged = DoubleVal(&config->latency[nodeIndex].loss01, 0.0, 1.0, "Loss % (0..1)") || anyChanged;
			if(ImGui::Checkbox("Allow out of order packets", &config->latency[nodeIndex].outOfOrder)) {
				anyChanged = true;
			}
			ImGui::PopID();
			return anyChanged;
		};
		settingsChanged = NodeSettings(s_sim.node + kNodeAutonomous, &g_config.simulation, kNodeAutonomous) || settingsChanged;
		ImGui::Separator();
		settingsChanged = NodeSettings(s_sim.node + kNodeAuthority, &g_config.simulation, kNodeAuthority) || settingsChanged;
	}
	ImGui::End();

	if(ImGui::Begin("Simulation Settings", (bool *)nullptr, 0)) {
		settingsChanged = DoubleVal(&g_config.simulation.duration, 1000.0, 100000.0, "Duration (ms)") || settingsChanged;
		settingsChanged = DoubleVal(&g_config.simulation.speed, 0.1, 10.0, "Revolutions") || settingsChanged;
		if(ImGui::Checkbox("Saved path", &g_config.simulation.savedPath)) {
			settingsChanged = true;
		}
		if(ImGui::Checkbox("Extrapolation", &g_config.simulation.useExtrapolation)) {
			settingsChanged = true;
		}
		if(ImGui::Button("Reset to defaults")) {
			settingsChanged = true;
			simulation_reset_config(&g_config);
		}
	}
	ImGui::End();

	if(ImGui::Begin("Smoothing Settings", (bool *)nullptr, 0)) {
		simulationNode *node;

		ImGui::Separator();
		node = s_sim.node + kNodeLerp;
		ImGui::PushID(node->name);
		ImGui::TextColored(ImColor(node->color), "%s", node->name);
		settingsChanged = FloatVal(&g_config.simulation.node[kNodeLerp].tuning, 0.0f, 1.0f, "Lerp Strength") || settingsChanged;
		settingsChanged = FloatVal(&g_config.simulation.node[kNodeLerp].maxExtrapolationMillis, 0.0f, 1000.0f, "Max Extrapolation (ms)") || settingsChanged;
		ImGui::PopID();

		ImGui::Separator();
		node = s_sim.node + kNodeExpLerp;
		ImGui::PushID(node->name);
		ImGui::TextColored(ImColor(node->color), "%s", node->name);
		settingsChanged = FloatVal(&g_config.simulation.node[kNodeExpLerp].tuning, 0.0f, 1.0f, "Lerp Strength") || settingsChanged;
		settingsChanged = FloatVal(&g_config.simulation.node[kNodeExpLerp].maxExtrapolationMillis, 0.0f, 1000.0f, "Max Extrapolation (ms)") || settingsChanged;
		ImGui::PopID();

		ImGui::Separator();
		node = s_sim.node + kNodeLerp2;
		ImGui::PushID(node->name);
		ImGui::TextColored(ImColor(node->color), "%s", node->name);
		settingsChanged = FloatVal(&g_config.simulation.node[kNodeLerp2].tuning, 0.0f, 1000.0f, "Delay (ms)") || settingsChanged;
		settingsChanged = FloatVal(&g_config.simulation.node[kNodeLerp2].maxExtrapolationMillis, 0.0f, 1000.0f, "Max Extrapolation (ms)") || settingsChanged;
		settingsChanged = FloatVal(&g_config.simulation.node[kNodeLerp2].extrapolationLerpStrength, 0.0f, 1.0f, "Extrapolation Lerp Strength") || settingsChanged;
		ImGui::PopID();
	}
	ImGui::End();

	if(settingsChanged) {
		simulation_reset(&s_sim);
		simulation_init(&s_sim, &g_config.simulation);
		simulation_generate(&s_sim);
		UISimulation_InitColors(&s_sim);
	}
}
