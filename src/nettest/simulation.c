// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#include "simulation.h"
#include "appdata.h"
#include "bb_array.h"
#include "bb_types.h"
#include "json_generated.h"
#include "structs_generated.h"

#define _USE_MATH_DEFINES
#include <float.h>
#include <math.h>

const double frameMillis = 1.0 / 60.0 * 1000.0;

void simulation_reset_config(config_t *config)
{
	for(int i = 0; i < 2; ++i) {
		latencySettings *latency = config->simulation.latency + i;
		latency->latencyMsec = 50.0;
		latency->jitterMsec = 50.0;
		latency->loss01 = 0.0;
		latency->outOfOrder = false;
	}
	config->simulation.duration = 10000.0;
	config->simulation.speed = 1.0;
	config->simulation.useExtrapolation = true;
	config->simulation.node[0].sendIntervalMillis = 1.0 / 60.0 * 1000.0;
	config->simulation.node[1].sendIntervalMillis = 1.0 / 10.0 * 1000.0;
	config->simulation.node[kNodeLerp].tuning = 0.1f;
	config->simulation.node[kNodeExpLerp].tuning = 0.15f;
	config->simulation.node[kNodeLerp2].tuning = 200.0f;
	for(int i = 0; i < kNodeCount; ++i) {
		config->simulation.node[i].visible = true;
		config->simulation.node[i].extrapolationLerpStrength = 0.1f;
		config->simulation.node[i].maxExtrapolationMillis = 500.0f;
	}
}

void simulation_init(simulation *sim, simulationConfig *config)
{
	simulation_reset(sim);
	memset(sim, 0, sizeof(*sim));
	sim->config = *config;

	for(u32 i = 0; i < kNodeCount; ++i) {
		sim->node[i].nodeSettings = config->node[i];
		sim->node[i].name = "unnamed";
	}

	pcg32_random baseSeed = random_make_pgc32_deterministic_seed();
	pcg32_random seed = baseSeed;

	simulationNode *node;

	node = sim->node + kNodeAutonomous;
	seed.inc = baseSeed.inc + 0;
	node->jitterStream = random_make_pcg32(seed);
	seed.inc = baseSeed.inc + 1;
	node->lossStream = random_make_pcg32(seed);
	node->latencySettings = config->latency[kNodeAutonomous];

	node = sim->node + kNodeAuthority;
	seed.inc = baseSeed.inc + 2;
	node->jitterStream = random_make_pcg32(seed);
	seed.inc = baseSeed.inc + 3;
	node->lossStream = random_make_pcg32(seed);
	node->latencySettings = config->latency[kNodeAuthority];

	sim->node[kNodeAutonomous].name = "Autonomous";
	sim->node[kNodeAuthority].name = "Authority";
	sim->node[kNodeSimulated].name = "Simulated";
	sim->node[kNodeLerp].name = "Lerp";
	sim->node[kNodeExpLerp].name = "ExpLerp";
	sim->node[kNodeLerp2].name = "Lerp2";
}

static void simulation_sendframe(simulation *sim, simulationNode *source, simulationNode *dest, simulationFrame *frame, double *lastReceiveTimestamp)
{
	double loss01 = random_get_float_01(&source->lossStream);
	if(loss01 < source->latencySettings.loss01)
		return;

	double jitter = source->latencySettings.jitterMsec * random_get_float_01(&source->jitterStream);
	double latency = source->latencySettings.latencyMsec + jitter;
	double targetTimestamp = frame->timestamp + latency;
	if(!source->latencySettings.outOfOrder && targetTimestamp < *lastReceiveTimestamp) {
		targetTimestamp = *lastReceiveTimestamp;
	}

	simulationFrame destFrame = *frame;
	destFrame.timestamp = targetTimestamp;
	bba_push(dest->frames, destFrame);
	*lastReceiveTimestamp = targetTimestamp;
	if(sim->duration < targetTimestamp) {
		sim->duration = targetTimestamp;
	}
}

static void simulation_send(simulation *sim, simulationNode *source, simulationNode *dest)
{
	double lastSendTimestamp = 0.0;
	double lastReceiveTimestamp = 0.0;
	double now = 0.0;
	u32 lastIndex = 0;
	simulationFrame *lastFrame = NULL;
	while(now < sim->duration) {
		if(lastSendTimestamp + source->nodeSettings.sendIntervalMillis < now) {
			simulationFrame *frame = simulation_findFrame(&source->frames, now, &lastIndex);
			if(lastFrame != frame) {
				lastFrame = frame;
				simulation_sendframe(sim, source, dest, frame, &lastReceiveTimestamp);
			}
			lastSendTimestamp = now;
		}
		now += frameMillis;
	}
}

static vec2d vec2d_mul(vec2d vec, float scalar)
{
	vec.x *= scalar;
	vec.y *= scalar;
	return vec;
}

static vec2d vec2d_div(vec2d vec, float scalar)
{
	vec.x /= scalar;
	vec.y /= scalar;
	return vec;
}

static vec2d vec2d_add(vec2d a, vec2d b)
{
	a.x += b.x;
	a.y += b.y;
	return a;
}

static vec2d vec2d_sub(vec2d a, vec2d b)
{
	a.x -= b.x;
	a.y -= b.y;
	return a;
}

static float lerp(float a, float b, float t)
{
	return a + (b - a) * t;
}

static vec2d vec2d_lerp(vec2d a, vec2d b, float t)
{
	a.x = lerp(a.x, b.x, t);
	a.y = lerp(a.y, b.y, t);
	return a;
}

static void simulation_smooth(simulation *sim, simulationNode *source, simulationNode *dest, nodeType smoothing)
{
	double now = 0.0;
	u32 lastIndex = 0;
	simulationFrame *prevNetFrame = NULL;
	vec2d prevPos = { BB_EMPTY_INITIALIZER };
	double prevNetFrameTime = 0.0;

	while(now < sim->duration) {
		simulationFrame *netFrame = simulation_findFrame(&source->frames, now, &lastIndex);
		if(netFrame) {
			if(!prevNetFrame) {
				prevPos = netFrame->pos;
				prevNetFrame = netFrame;
				prevNetFrameTime = now;
			} else if(netFrame->id > prevNetFrame->id) {
				prevNetFrame = netFrame;
				prevNetFrameTime = now;
			}

			simulationFrame frame = { BB_EMPTY_INITIALIZER };
			frame.timestamp = now;
			frame.id = netFrame->id;

			vec2d targetPos = netFrame->pos;
			if(sim->config.useExtrapolation) {
				float extrapolationDuration = (float)(now - prevNetFrameTime);
				if(extrapolationDuration > dest->nodeSettings.maxExtrapolationMillis) {
					extrapolationDuration = dest->nodeSettings.maxExtrapolationMillis;
				}
				extrapolationDuration *= 0.001f;
				targetPos = vec2d_add(netFrame->pos, vec2d_mul(netFrame->vel, extrapolationDuration));
			}

			if(smoothing == kNodeLerp) {
				frame.pos = vec2d_add(vec2d_mul(prevPos, 1.0f - dest->nodeSettings.tuning), vec2d_mul(targetPos, dest->nodeSettings.tuning));
			} else if(smoothing == kNodeExpLerp) {
				// see https://www.gamasutra.com/blogs/ScottLembcke/20180404/316046/Improved_Lerp_Smoothing.php
				float tuning = (float)exp(-dest->nodeSettings.tuning * frameMillis);
				frame.pos = vec2d_add(vec2d_mul(prevPos, 1.0f - tuning), vec2d_mul(targetPos, tuning));
			}
			bba_push(dest->frames, frame);
			prevPos = frame.pos;
		}

		now += frameMillis;
	}
}

// Given the reconstructed send timestamp, find the frames with estimated send times
// before that time and equal to or later than that time.
bool simulation_findFrames(simulationFrames *frames, double now, double reconstructedTimestamp, u32 *lastStartIndex, simulationFrame **prevFrame, simulationFrame **nextFrame)
{
	if(!frames->count) {
		return false;
	}

	u32 startIndex = 0;
	if(lastStartIndex && *lastStartIndex < frames->count) {
		startIndex = *lastStartIndex;
	}

	if(frames->data[startIndex].timestamp > now) {
		startIndex = 0;
	}

	for(u32 i = startIndex; i < frames->count; ++i) {
		simulationFrame *frame = frames->data + i;
		if(frame->timestamp > now)
			break;

		double frameSendTimestamp = frame->id * frameMillis;
		if(frameSendTimestamp < reconstructedTimestamp) {
			*prevFrame = frame;
			if(lastStartIndex) {
				*lastStartIndex = i;
			}
		} else {
			*nextFrame = frame;
			break;
		}
	}

	return *prevFrame != NULL && *nextFrame != NULL;
}

static void simulation_smooth_lerp2(simulation *sim, simulationNode *source, simulationNode *dest)
{
	double now = 0.0;
	u32 lastIndex = 0;
	u32 lastEstimatedIndex = 0;
	simulationFrame *prevNetFrame = NULL;
	simulationFrame *curNetFrame = NULL;
	vec2d curPos = { BB_EMPTY_INITIALIZER };
	u32 frameCount = 0;
	b32 foundFrames = false;

	b32 extrapolating = false;
	double extrapolatingFrameTime = 0.0;
	vec2d extrapolationPos = { BB_EMPTY_INITIALIZER };
	u32 lastExtrapolationEstimatedIndex = 0;
	vec2d extrapolationError = { BB_EMPTY_INITIALIZER };

	while(now < sim->duration) {
		double targetTime = now;
		++frameCount;
		simulationFrame *netFrame = simulation_findFrame(&source->frames, targetTime, &lastIndex);
		if(netFrame) {
			if(!curNetFrame) {
				curPos = netFrame->pos;
				prevNetFrame = curNetFrame;
				curNetFrame = netFrame;
				dest->targetLatency = (float)now; // #TODO: latency estimation
			} else if(netFrame->id > curNetFrame->id) {
				prevNetFrame = curNetFrame;
				curNetFrame = netFrame;
			}

			simulationFrame frame = { BB_EMPTY_INITIALIZER };
			frame.timestamp = now;
			frame.id = netFrame->id;

			// We are at time 'now', and we want to find the frames before and after the original time 'now - backup'.
			// We don't have the original timestamp - the frames' timestamps are the receive times.  So, we reconstruct
			// the original timestamps by multiplying id by frameMillis.  This will have to change if we don't want to
			// fix client input stream at 60hz.

			//double backup = ((dest->targetLatency + dest->nodeSettings.tuning) / frameMillis) + 0.5;
			double backup = /*dest->targetLatency +*/ dest->nodeSettings.tuning;
			double sendTime = now - backup;
			if(sendTime < 0) {
				frame.pos = curNetFrame->pos;
				frame.flags |= kFrameFlagOutOfData;
			} else {

				simulationFrame *prevFrame = NULL;
				simulationFrame *nextFrame = NULL;

				if(extrapolating) {
					if(simulation_findFrames(&source->frames, now, extrapolatingFrameTime, &lastExtrapolationEstimatedIndex, &prevFrame, &nextFrame)) {
						extrapolating = false;
						double prevTime = prevFrame->id * frameMillis;
						double nextTime = nextFrame->id * frameMillis;
						double t = (extrapolatingFrameTime - prevTime) / (nextTime - prevTime);
						vec2d resolvedPos = vec2d_lerp(prevFrame->pos, nextFrame->pos, (float)t);
						extrapolationError = vec2d_sub(extrapolationPos, resolvedPos);
					}

					prevFrame = NULL;
					nextFrame = NULL;
				}

				if(simulation_findFrames(&source->frames, now, sendTime, &lastEstimatedIndex, &prevFrame, &nextFrame)) {

					foundFrames = true;

					// now we estimate when the prev and next frames were generated, so we have a lerp t
					double prevTime = prevFrame->id * frameMillis;
					double nextTime = nextFrame->id * frameMillis;
					double t = (sendTime - prevTime) / (nextTime - prevTime);
					frame.pos = vec2d_lerp(prevFrame->pos, nextFrame->pos, (float)t);

				} else if(foundFrames) {

					if(sim->config.useExtrapolation) {
						vec2d vel = prevFrame->vel;
						vec2d accel = { BB_EMPTY_INITIALIZER };
						if(prevFrame > source->frames.data) {
							// extrapolate 2 previous frames' vel
							simulationFrame *prevPrevFrame = prevFrame - 1;
							accel = vec2d_div(vec2d_sub(prevFrame->vel, prevPrevFrame->vel), (float)(prevFrame->timestamp - prevPrevFrame->timestamp));
						}

						double prevTime = prevFrame->id * frameMillis;
						double extrapolationTime = sendTime - prevTime;
						if(extrapolationTime > dest->nodeSettings.maxExtrapolationMillis) {
							extrapolationTime = dest->nodeSettings.maxExtrapolationMillis; // don't extrapolate forever...
						}
						extrapolationTime *= 0.001;
						frame.pos = vec2d_add(vec2d_add(prevFrame->pos, vec2d_mul(vel, (float)extrapolationTime)), vec2d_mul(accel, (float)(extrapolationTime * extrapolationTime * 0.5)));
						frame.flags |= kFrameFlagOutOfData;

						extrapolating = true;
						extrapolatingFrameTime = sendTime;
						extrapolationPos = frame.pos;
					} else {
						frame.pos = prevFrame->pos;
						frame.flags |= kFrameFlagOutOfData;
					}

				} else {
					// initial hold until we have frames to start lerping between
					frame.pos = source->frames.data[0].pos;
					frame.flags |= kFrameFlagOutOfData;
				}

				vec2d extrapolationFixup = vec2d_mul(extrapolationError, dest->nodeSettings.extrapolationLerpStrength);
				extrapolationError = vec2d_sub(extrapolationError, extrapolationFixup);
				frame.pos = vec2d_add(frame.pos, extrapolationError);
			}

			bba_push(dest->frames, frame);
			curPos = frame.pos;
		}

		now += frameMillis;
	}
}

void simulation_generate(simulation *sim)
{
	simulation_reset(sim);
	sim->duration = sim->config.duration;
	sim->entityRadius = 45.0f;

	// Generate autonomous client's frames
	u32 frameId = 1;
	double autonomousClock = 0.0;
	simulationNode *node = sim->node + kNodeAutonomous;
	if(sim->config.savedPath) {
		sb_t path = appdata_get("nettest");
		sb_append(&path, "\\path.json");
		JSON_Value *val = json_parse_file(sb_get(&path));
		if(val) {
			savedMoves moves = json_deserialize_savedMoves(val);
			json_value_free(val);

			for(u32 i = 0; i < moves.count; ++i) {
				savedMove *move = moves.data + i;
				autonomousClock = frameMillis * i;
				frameId = i + 1;

				simulationFrame frame = { BB_EMPTY_INITIALIZER };
				frame.timestamp = autonomousClock;
				frame.id = frameId;
				frame.pos.x = move->x;
				frame.pos.y = move->y;
				if(node->frames.count) {
					simulationFrame *prev = node->frames.data + node->frames.count - 1;
					frame.vel = vec2d_div(vec2d_sub(frame.pos, prev->pos), (float)((frame.timestamp - prev->timestamp) / 1000.0));
				}

				bba_push(node->frames, frame);
			}

			savedMoves_reset(&moves);
		}
		sb_reset(&path);
	} else {
		while(autonomousClock < sim->config.duration) {
			simulationFrame frame = { BB_EMPTY_INITIALIZER };
			frame.timestamp = autonomousClock;
			frame.id = frameId;
			frame.pos.x = 1000.0f * (float)cos(2.0 * M_PI * frame.timestamp / sim->config.duration * sim->config.speed);
			frame.pos.y = 1000.0f * (float)sin(2.0 * M_PI * frame.timestamp / sim->config.duration * sim->config.speed);
			if(node->frames.count) {
				simulationFrame *prev = node->frames.data + node->frames.count - 1;
				frame.vel = vec2d_div(vec2d_sub(frame.pos, prev->pos), (float)((frame.timestamp - prev->timestamp) / 1000.0));
			}

			bba_push(node->frames, frame);

			++frameId;
			autonomousClock += frameMillis;
		}
	}

	// Send autonomous to authority
	simulation_send(sim, sim->node + kNodeAutonomous, sim->node + kNodeAuthority);

	// Send authority to simulated
	simulation_send(sim, sim->node + kNodeAuthority, sim->node + kNodeSimulated);

	// Apply smoothing to simulated results
	for(u32 i = kNodeLerp; i < kNodeCount; ++i) {
		if(i == kNodeLerp2) {
			simulation_smooth_lerp2(sim, sim->node + kNodeSimulated, sim->node + i);
		} else {
			simulation_smooth(sim, sim->node + kNodeSimulated, sim->node + i, i);
		}
	}

	// Find bounds for UI scaling
	sim->boundsMin.x = FLT_MAX;
	sim->boundsMin.y = FLT_MAX;
	sim->boundsMax.x = -FLT_MAX;
	sim->boundsMax.y = -FLT_MAX;
	for(u32 nodeIndex = 0; nodeIndex < kNodeCount; ++nodeIndex) {
		node = sim->node + nodeIndex;
		for(u32 frameIndex = 0; frameIndex < node->frames.count; ++frameIndex) {
			simulationFrame *frame = node->frames.data + frameIndex;
			if(sim->boundsMin.x > frame->pos.x) {
				sim->boundsMin.x = frame->pos.x;
			}
			if(sim->boundsMin.y > frame->pos.y) {
				sim->boundsMin.y = frame->pos.y;
			}
			if(sim->boundsMax.x < frame->pos.x) {
				sim->boundsMax.x = frame->pos.x;
			}
			if(sim->boundsMax.y < frame->pos.y) {
				sim->boundsMax.y = frame->pos.y;
			}
		}
	}
}

void simulation_reset(simulation *sim)
{
	for(u32 i = 0; i < kNodeCount; ++i) {
		bba_free(sim->node[i].frames);
	}
}

simulationFrame *simulation_findFrame(simulationFrames *frames, double timestamp, u32 *lastStartIndex)
{
	if(!frames->count) {
		return NULL;
	}

	u32 startIndex = 0;
	if(lastStartIndex && *lastStartIndex < frames->count) {
		startIndex = *lastStartIndex;
	}

	if(frames->data[startIndex].timestamp > timestamp) {
		startIndex = 0;
	}

	u32 bestFrameIndex = startIndex;
	simulationFrame *bestFrame = NULL;
	for(u32 i = startIndex; i < frames->count; ++i) {
		simulationFrame *frame = frames->data + i;
		if(frame->timestamp <= timestamp) {
			bestFrame = frame;
			bestFrameIndex = i;
		} else {
			break;
		}
	}

	if(lastStartIndex) {
		*lastStartIndex = bestFrameIndex;
	}
	return bestFrame;
}
