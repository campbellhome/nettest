// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#include "config.h"
#include "appdata.h"
#include "bb_array.h"
#include "bb_string.h"
#include "bb_wrap_stdio.h"
#include "common.h"
#include "env_utils.h"
#include "file_utils.h"
#include "json_generated.h"
#include "line_parser.h"
#include "process_task.h"
#include "sb.h"
#include "simulation.h"
#include "span.h"
#include "structs_generated.h"
#include "tokenize.h"
#include "va.h"
#include <stdlib.h>

config_t g_config;

void config_getwindowplacement(HWND hwnd)
{
	UINT oldShowCmd = g_config.wp.showCmd;
	memset(&g_config.wp, 0, sizeof(g_config.wp));
	g_config.wp.length = sizeof(g_config.wp);
	GetWindowPlacement(hwnd, &g_config.wp);
	if(g_config.wp.showCmd == SW_SHOWMINIMIZED) {
		g_config.wp.showCmd = oldShowCmd;
	}
}

void config_free(config_t *config)
{
	config_reset(config);
	free(config);
}

static sb_t config_get_path(const char *appName)
{
	sb_t s = appdata_get(appName);
	sb_append(&s, "\\nettest_config.json");
	return s;
}

b32 config_read(config_t *config)
{
	b32 ret = false;
	sb_t path = config_get_path("nettest");
	JSON_Value *val = json_parse_file(sb_get(&path));
	if(val) {
		*config = json_deserialize_config_t(val);
		json_value_free(val);
		ret = true;
	}
	sb_reset(&path);

	if(config->wp.showCmd == SW_SHOWMINIMIZED ||
	   config->wp.showCmd == SW_MINIMIZE ||
	   config->wp.showCmd == SW_SHOWMINNOACTIVE ||
	   config->wp.showCmd == SW_FORCEMINIMIZE) {
		config->wp.showCmd = SW_NORMAL;
	}
	if(config->version == 0) {
		config->doubleClickSeconds = 0.3f;
		config->dpiAware = true;
		config->dpiScale = 1.0f;
		config->uiFontConfig.enabled = true;
		config->uiFontConfig.size = 16;
		sb_append(&config->uiFontConfig.path, "C:\\Windows\\Fonts\\verdana.ttf");
		config->uiFontConfig.enabled = true;
		sb_append(&config->colorscheme, "ImGui Dark");
	}
	if(config->version < 3) {
		simulation_reset_config(config);
	}
	if(config->version < 4) {
		for(u32 i = 0; i < kNodeCount; ++i) {
			config->simulation.node[i].extrapolationLerpStrength = 0.1f;
			config->simulation.node[i].maxExtrapolationMillis = 500.0f;
		}
	}
	config->version = kConfigVersion;
	return ret;
}

b32 config_write(config_t *config)
{
	b32 result = false;
	JSON_Value *val = json_serialize_config_t(config);
	if(val) {
		sb_t path = config_get_path("nettest");
		FILE *fp = fopen(sb_get(&path), "wb");
		if(fp) {
			char *serialized_string = json_serialize_to_string_pretty(val);
			fputs(serialized_string, fp);
			fclose(fp);
			json_free_serialized_string(serialized_string);
			result = true;
		}
		sb_reset(&path);
	}
	json_value_free(val);
	return result;
}
